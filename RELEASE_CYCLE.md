# **MetaSat Release Cycle About Page**

# **Release Cycle**

Due to MetaSat being in early stages of development, [releases](https://gitlab.com/metasat/metasat-toolkit/-/releases) do not yet follow a strict cycle. However, as development becomes more stable and updates less frequent (see Version Numbering section below), the MetaSat release cycle will be revised in order to provide more consistency.Updated release cycles will include evaluation and implementation periods, in addition to more concrete timelines and curatorial roles.


# **Version Numbering**

MetaSat Toolkit [releases](https://gitlab.com/metasat/metasat-toolkit/-/releases) adhere to the [Semantic Versioning (SemVer) Specification](https://semver.org/) and are formatted as &#39;MAJOR.MINOR.UPDATE&#39; (e.g., v.1.3.5):


- **MAJOR** changes to MetaSat include:

  - Addition or Depreciation of MetaSat [Concepts](https://schema.space/metasat/)
  - Addition or Deprecation of MetaSat [Tools](https://schema.space/resources#tools)
  - Changes to Concept IDs and/or Concept URIs


- **MINOR** changes to MetaSat include:

  - Addition or Deprecation of MetaSat [Crosswalks](https://schema.space/crosswalk/)
  - Concept Edits:
    - Concept Name (NOT ID)
    - Concept Description and/or Description Source
    - Concept Example Value
    - Concept Synonyms
    - Redesignation of Concept [Segments](https://schema.space/metasat/?view=segment)
    - Redesignation of Concept [Families](https://schema.space/metasat/?view=family)


- **UPDATE** includes changes to MetaSat resources such as:

  - Documentation
    - [MetaSat Website](https://schema.space/)
      - [Resources Page](https://schema.space/resources)
      - [About Page](https://schema.space/about)
    - [MetaSat GitLab Repository](https://gitlab.com/metasat/metasat-toolkit/)
