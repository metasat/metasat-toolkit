# Contributing to MetaSat

Welcome! As a community-driven and openly developed project, all contributions to the MetaSat toolkit are welcomed and regularly incorporated into MetaSat releases.

If you are interested in lending a hand, please consider:

[Adding MetaSat Concepts](https://gitlab.com/metasat/metasat-toolkit/-/blob/master/CONTRIBUTING.md#adding-metasat-concepts)

[Reviewing MetaSat Concepts](https://gitlab.com/metasat/metasat-toolkit/-/blob/master/CONTRIBUTING.md#reviewing-metasat-concepts)

[Testing MetaSat Concepts URIs](https://gitlab.com/metasat/metasat-toolkit/-/blob/master/CONTRIBUTING.md#testing-metasat-concepts-uris)

[Building Crosswalks](https://gitlab.com/metasat/metasat-toolkit/-/blob/master/CONTRIBUTING.md#building-crosswalks)

[Becoming a MetaSat Subject Specialist](https://gitlab.com/metasat/metasat-toolkit/-/blob/master/CONTRIBUTING.md#becoming-a-metasat-subject-specialist)

Note that we have adopted a Code of Conduct that contributors to the MetaSat project are expected to adhere to. You can see our Code of Conduct [here](https://gitlab.com/metasat/metasat-toolkit/-/blob/master/CODE_OF_CONDUCT.md).

To learn more about MetaSat versioning, check out our [release cycle about page](https://gitlab.com/metasat/metasat-toolkit/-/blob/master/RELEASE_CYCLE.md).

* * * * *

# Adding MetaSat Concepts

All concepts in the MetaSat vocabulary have been added through input and feedback from the satellite community. If you see that we are missing a concept in the vocabulary, please submit an [issue](https://gitlab.com/metasat/metasat-toolkit/-/issues) on GitLab using our [new concept template](https://gitlab.com/metasat/metasat-toolkit/-/blob/master/issue_templates/adding_concepts.md) with your suggestion to start the process of getting your concept added  to the vocabulary in our next [release](https://gitlab.com/metasat/metasat-toolkit/-/releases)!

* * * * *

# Reviewing MetaSat Concepts

Each MetaSat concept has many attributes that help users determine if it's right for their particular use-case. Due to the large number of concepts, attributes for concepts are prone to error (e.g., inaccuracies, spelling mistakes, etc.). Please submit an [issue](https://gitlab.com/metasat/metasat-toolkit/-/issues) on GitLab using our [reviewing concepts template](https://gitlab.com/metasat/metasat-toolkit/-/blob/master/issue_templates/reviewing_concepts.md) if you have a suggestion on one of the following concept attributes:

-   MetaSat ID

-   Concept Name

-   Description and/or Description Source

-   Example Value

-   Synonym(s)

-   Concept Segment(s)

-   Concept Family/Families


* * * * *

# Testing MetaSat Concepts URIs


Does your project already utilize space mission terms? If so, try incorporating MetaSat concept URIs ([what are these?](https://gitlab.com/metasat/metasat-toolkit/-/blob/master/docs/URLs_semantic_web.md)) into your services. Examples include:

-   Using MetaSat concept URIs in your metadata schemas and models to better structure your data and make it machine readable

-   MetaSat concept URIs are optimized for JSON-LD metadata schemas ([learn more](https://gitlab.com/metasat/metasat-toolkit/-/blob/master/docs/json-ld_primer.md)) but may also be used in RDF/XML, N-Triples, Turtle, etc.

-   Using MetaSat concept URIs to name and model your API resources

-   Embedding MetaSat URIs in your website or documentation

* * * * *

# Building Crosswalks


Having many [crosswalks](https://schema.space/crosswalk/) between concepts in MetaSat and other vocabularies (e.g., Wikidata) is essential to the toolkit's success as a semantic web technology.

The process of [crosswalking](https://gitlab.com/metasat/metasat-toolkit/-/blob/master/docs/getting_started.md) to MetaSat concept URIs can be fun!  So if you know of an existing vocabulary that we haven't yet crosswalked too already, please let us know by [submitting an issue](https://gitlab.com/metasat/metasat-toolkit/-/issues) using our [building crosswalks template](https://gitlab.com/metasat/metasat-toolkit/-/blob/master/issue_templates/building_crosswalks.md). You can also suggest edits to our existing crosswalks too.

Also feel free to build your own crosswalks to MetaSat. The best way to submit these are to [send us](https://schema.space/contact) tables that match concepts like the example below (which crosswalks MetaSat and Wikidata properties):

| MetaSat Concept                                           | Wikidata Property                           |
|-----------------------------------------------------------|--------------------------------------------:|
|[orbitType](https://schema.space/metasat/orbitType)        |[P522](http://www.wikidata.org/entity/P522)  |
|[orbitalPeriod](https://schema.space/metasat/orbitalPeriod)|[P2146](http://www.wikidata.org/entity/P2146)|

* * * * *

# Becoming a MetaSat Subject Specialist

MetaSat has been built by contributions and feedback from an incredibly diverse group of people, all with their own levels of expertise and unique areas of interest.

Given the diversity of feedback received already, and the immense subject area MetaSat anticipates to cover, ensuring that topics in the vocabulary are equally represented and proportionate to one another has become a primary area of focus.

To address this issue and to make the MetaSat toolkit maximally comprehensive, we encourage individuals with specific areas of expertise to add and review MetaSat concepts that pertain to their specific proficiencies.

If you are interested in becoming a MetaSat Subject Specialist, please [let us know](https://schema.space/contact)!
