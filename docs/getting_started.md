# Getting Started With MetaSat  

The MetaSat vocabulary is the core of the MetaSat project. The MetaSat vocabulary is a list of unique concepts that can be used to describe spacecraft, missions, ground stations, and more, each with a unique, permanent URI. A URI, or **U**niform **R**esource **I**dentifier, acts as a machine-readable identifier for the concept. The MetaSat vocabulary and its URIs can be used can be used to describe missions both in private databases and on the web; since each concept uses its own URI, the vocabulary can be used for linked data applications and schemas that use any format of the [RDF data model](https://gitlab.com/metasat/metasat-toolkit/-/blob/master/docs/RDF.md), including RDF-XML, Turtle, N-Triples, and more.  

We recommend using the MetaSat vocabulary with [JSON-LD](https://json-ld.org/), a highly flexible form of RDF that is built to be easily human-writable and machine-readable. To illustrate how JSON-LD can be used with MetaSat, our GitLab repository has multiple [example files](https://gitlab.com/metasat/metasat-toolkit/-/tree/master/examples) that describe real missions. They include descriptions of satellites and missions, as well as ground stations and observations. These files can be used to inspire your own use of JSON-LD with MetaSat.   

MetaSat is useful when describing items in databases such as NASA's [SPOON](https://spoonsite.com/) (SmallSat Parts On Orbit Now) because our URIs can be referenced directly in any schema without changing its underlying structure. For example, existing entity-relationship models and naming conventions can be retained even when incorporating MetaSat URIs. This way, the schema referred to by your database doesn't need to be a new JSON-LD schema developed by MetaSat.  

## Context and Crosswalks  

### Context  

The “magic” of JSON-LD, and what separates it from plain JSON, is the `@context` section at the beginning. The `@context` section lets you define a group of aliases that can then be used to expand each “key,” or attribute name, in the document into a machine-readable URI.  

For example, if you  add the line `"@vocab": "https://schema.space/metasat/"` to your `@context` section, this will allow each key you use to expand to https://schema.space/metasat/exampleKey (e.g., "dynamicalTime" will expand to https://schema.space/metasat/dynamicalTime).  

You can also include other vocabularies in your `@context` by creating aliases for their main URLs and using [CURIEs](https://en.wikipedia.org/wiki/CURIE). For example, if the `@context` includes the line `"schema": "https://schema.org/"`, a key such as “schema:Person” would expand to https://schema.org/Person. 

Here is a simple example, describing [a SatNOGS observation](https://network.satnogs.org/observations/2243157/), that combines each of these ideas:  

``` JSON
{
    "@context": {
        "@vocab": "https://schema.space/metasat/",
        "schema": "https://schema.org/"
    },
    "@id": "https://network.satnogs.org/observations/2243157/",
    "schema:identifier": "2243157",
    "groundStation": "1378 - Wolbach Library"
}
```

`“@id”` is a JSON-LD specific concept that stores a URI for what is being described; in this example, it is the SatNOGS observation we are interested in. The code above, when run through an [expansion algorithm](https://gitlab.com/metasat/metasat-toolkit/-/blob/master/docs/json-ld_primer.md#expansion), will expand to:  

``` JSON
{
    "@id": "https://network.satnogs.org/observations/2243157/",
    "https://schema.space/metasat/groundStation": [
    {
        "@value": "1378 - Wolbach Library"
    }
    ],
    "https://schema.org/identifier": [
    {
        "@value": "2243157"
    }
    ]
}
```
Notice that “groundStation” was expanded to https://schema.space/metasat/groundStation, and “schema:identifier” was expanded to https://schema.org/identifier. The human-readable keys were transformed into machine-readable URIs!  

Linked data, often using JSON-LD, is also used for search engine optimization (SEO). For example, a search engine like Google might use JSON-LD metadata to inform its “[Knowledge Graphs](https://en.wikipedia.org/wiki/Knowledge_Graph),” which aggregate often-searched information about popular topics such as celebrities, businesses, and more. If you have ever Googled something like “How long is a marathon?” or "How tall is Shaquille O'Neal?" and found the answer before any links show up, you have taken advantage of linked data!  Having structured and linked metadata is also crucial to ranking well in search engine indices.  

Ranking well in search engine indices helps the public and scientists alike find what they need to do their work, but this is also valuable to vendors. Search engine ranking is often the deciding factor of whether or not a sale is made by a vendor or their competitor. Good SEO through structured and linked metadata can therefore be thought of as a free form of advertising which takes full advantage of the emerging semantic web.

### Crosswalking  

We are also hard at work building crosswalks for MetaSat. A metadata crosswalk is a set of instructions for converting metadata of one schema or vocabulary into another. Using JSON-LD, it is easy to create crosswalks between different linked data vocabularies, since you just have to update the `@context` section. You can even combine different vocabularies into a single document, or convert into another RDF syntax, such as Turtle or RDF-XML, without losing any information.  

We are working on making conversions between different vocabularies even easier by building a full MetaSat [Crosswalk](https://schema.space/crosswalk). Many of our concepts are already crosswalked to the popular schema.org vocabulary, as well as to WikiData items and properties as available.  We are currently working on building connections to other vocabularies, schemas, standards, thesauri, and taxonomies, as well.  

| MetaSat concept | WikiData Property | Schema.org |
| ------- | ----------------- | ---------- |
| [duration](https://schema.space/metasat/duration) | [P2047](https://www.wikidata.org/wiki/Property:P2047) | [Duration](https://schema.org/Duration) |
| [mass](https://schema.space/metasat/mass) | [P2067](https://www.wikidata.org/wiki/Property:P2067) | [Mass](https://schema.org/Mass) |

**We encourage linking out to existing URIs when possible. This type of flexibility, combined with MetaSat being an open source project, is particularly beneficial to those who only want to adopt a portion of MetaSat's Vocabulary. In this way, MetaSat remains neutral even when adopted by parties competing against each other such as vendors.**  

Lastly, by crosswalking to MetaSat URIs, issues of language and the transfer of information in databases, product datasheets, specifications, etc., quickly diminish. If, for example, vendors in the U.K. wish to submit thermal “louvres” to the user-generated SPOON database, customers in the U.S will recognize these products as thermal “louvers” in American English. These types of one-to-one connections are made possible by the MetaSat crosswalk and are particularly important when comparing product specifications that utilize different units and measurement systems.  

## Example Files

In our GitLab repository, you will find a folder with [example MetaSat files](https://gitlab.com/metasat/metasat-toolkit/-/tree/master/examples). These files are complete MetaSat JSON-LD files that describe real space missions, satellites, ground stations, and more. These files combine a number of linked data concepts, including combining vocabularies, using `@id` in JSON-LD, and linking concepts within a JSON-LD file.  

These files are only examples, and do not reflect the only way that MetaSat and JSON-LD could be used to describe their concepts.  

Unless specified otherwise in the file name, the example files describe entities that can be found in the [SatNOGS network](https://network.satnogs.org/) or [SatNOGS DB](https://db.satnogs.org/).  

> written by Allie Williams
