# Documents

This folder contains education materials to help make it easier to implement MetaSat, linked data, and JSON-LD. For requests or clarification, please feel free to submit an issue to this repository.

- [RDF.md](https://gitlab.com/metasat/metasat-toolkit/-/blob/master/docs/RDF.md) contains a rundown of the Resource Description Framework (RDF), semantic triples, and some of the functionality of the JSON-LD syntax.

- [URLs_semantic_web.md](https://gitlab.com/metasat/metasat-toolkit/-/blob/master/docs/URLs_semantic_web.md) contains a primer about how URLs work and why they are used by semantic web technologies.

- [about_metasat.md](https://gitlab.com/metasat/metasat-toolkit/-/blob/master/docs/about_metasat.md) contains a copy of the About page found on the MetaSat website at https://schema.space/about.

- [getting_started.md](https://gitlab.com/metasat/metasat-toolkit/-/blob/master/docs/getting_started.md) contains information needed to start using the MetaSat vocabulary in addition to explanatory material on linked data.

- [json-ld_primer.md](https://gitlab.com/metasat/metasat-toolkit/-/blob/master/docs/json-ld_primer.md) contains an introduction to using the JSON-LD syntax. It also includes information on JSON-LD transformation algorithms.
