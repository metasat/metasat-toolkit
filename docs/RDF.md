# Resource Description Framework (RDF)

The [**R**esource **D**escription **F**ramework](https://www.w3.org/TR/rdf11-primer/), or RDF, is a data model that is used to describe metadata on the web. RDF stores metadata in machine-readable “semantic triples” that are each made up of a subject, a predicate, and an object, much like a natural language sentence. However, RDF triples work a little bit differently than natural language. For example, in the sentence “My car is silver,” the subject is “my car,” the predicate is “is,” and the object is “silver.” As a human reading that sentence, you know that I am telling you the color of my car, but this isn’t obvious to a machine&mdash;the word “is” can mean a lot of things! An RDF triple that holds the same information might look like: “**My car (subject)** *has color (predicate)* ***silver (object)***.” This combination of subject, predicate, and object holds more meaning to a machine because you are explicitly telling it that you are talking about color.  

RDF can be written in several different syntaxes, many of which clearly store information in triples. You can also store semantic triples in JSON-LD, which is very similar to [JSON](https://www.json.org/json-en.html) syntax (in fact, *all valid JSON-LD is also valid JSON*). It is convenient to store information in JSON syntax because it is easy for computers to read and understand data that is structured. Both JSON and JSON-LD store information in key-value pairs. For example, storing information about color in JSON syntax would look like this:  

``` JSON
{
    "color": "silver"
}
```  

In JSON syntax, the first concept (“color”) is called the key, and the second concept (“silver”) is the value. But how do we get from semantic *triples* to key-value *pairs*? An easy way is to use JSON’s nesting capabilities:  

``` JSON
{
    "myCar": {
        "color": "silver"
    }
}
```  

Now, even though “myCar” and “color” are both keys, “myCar” acts as the subject and “color” as the predicate. (There is another way to create triples in JSON-LD using the `@id` term. This is explained a few paragraphs down)  

JSON-LD helps us make this syntax even more machine-friendly by using Uniform Resource Identifiers, or URIs. A URI can act as a unique, machine-readable and web-friendly identifier for an item or resource. To make proper machine-readable linked data, at least the predicate keys should use URIs. In this case, a good URI to use for the idea “has color” is https://schema.org/color. This is a good URI because it provides the program with both a unique, permanent identifier for the idea and a definition of the concept (“The color of the product.”).  

JSON-LD uses a special section called the `@context` section to define how human-readable keys can be [expanded](https://gitlab.com/metasat/metasat-toolkit/-/blob/master/docs/json-ld_primer.md#expansion) into machine-readable URIs. One especially useful concept that can be used in the `@context` is `@vocab`. This concept defines the base vocabulary of the file. Most keys will expand so that they are concatenated to the end of the URI specified by `@vocab`. In the following example, the `@context` section contains the line `"@vocab": "https://schema.org/"`, so the key “color” is expanded to https://schema.org/color. (note that the slash after https://schema.org is necessary; otherwise the key will expand to https://schema.orgcolor. Make sure to always include the delimiter at the end when using this strategy!)  

After using these techniques, our new semantic triple will be “**My car** *https://schema.org/color* ***silver***.” A JSON-LD file holding this triple might look like:  

``` JSON 
{
    "@context": {
        "@vocab": "https://schema.org/"
    },
    "color": "silver"
}
```  

In this case, you would have to know that this is metadata describing my car--maybe the file is named “myCar.jsonld.”  

We can also define a URI for the subject of the triple. Since there isn’t a URI for my personal car, we’re going to look at a [SatNOGS observation](https://network.satnogs.org/observations/2243157/) for the rest of the examples. We want to make a JSON-LD file to store the triple “**This specific observation** *has ground station* ***1378 - Wolbach Library***.” MetaSat has a URI for the concept “has ground station”: https://schema.space/metasat/groundStation. In addition, the observation has a unique URI: https://network.satnogs.org/observations/2243157/. How do we store the URI for the subject using JSON-LD?  

There is a special concept that can be used with JSON-LD to store the identifier for the subject: `@id`. `@id` can ONLY store URIs for its value. If anything else is stored in the value of `@id`, it will be expanded using the base vocabulary. Using `@id` to store the URI, our JSON-LD file for the observation will look like this (notice that the base vocabulary was changed to https://schema.space/metasat/):  

``` JSON
{
    "@context": {
        "@vocab": "https://schema.space/metasat/"
    },
    "@id": "https://network.satnogs.org/observations/2243157/",
    "groundStation": "1378 - Wolbach Library"
}
```  

Now the machine-readable RDF triple is “**https://network.satnogs.org/observations/2243157/** *https://schema.space/metasat/groundStation* ***1378 - Wolbach Library***.”  

What if we want to use concepts that are not in the MetaSat vocabulary? You can also include other vocabularies by creating aliases for their base URIs and using [CURIEs](https://en.wikipedia.org/wiki/CURIE). For example, the `@context` section could include the line `"schema": "https://schema.org/"`; then, a key such as “schema:Person” would expand to https://schema.org/Person.  

Now we can easily add another triple using schema.org’s “Identifier” predicate. This JSON-LD file will look like:  

``` JSON
{
    "@context": {
        "@vocab": "https://schema.space/metasat/",
        "schema": "https://schema.org/"
    },
    "@id": "https://network.satnogs.org/observations/2243157/",
    "schema:identifier": "2243157",
    "groundStation": "1378 - Wolbach Library"
}
```  

Just like plain JSON, JSON-LD can be used to nest information to make it look more hierarchical. JSON-LD isn’t inherently hierarchical, but using nesting can help make files more human-readable. Right now, the ground station is an object with respect to the observation, but we can also include triples where the ground station is a subject. We can use `@id` to make the ground station’s URI a subject, then add another predicate and object. For example, maybe we want to add a triple that says “**This ground station** *has name* ***1378 - Wolbach Library***.” The file would look like:  

``` JSON
{
    "@context": {
        "@vocab": "https://schema.space/metasat/",
        "schema": "https://schema.org/"
    },
    "@id": "https://network.satnogs.org/observations/2243157/",
    "schema:identifier": "2243157",
    "groundStation": {
        "@id": "https://network.satnogs.org/stations/1378/",
        "schema:name": "1378 - Wolbach Library"
    }
}
```  

Note that curly braces are used to denote a new JSON-LD object. The new triple, in machine-readable form, is “**https://network.satnogs.org/stations/1378/** *https://schema.org/name* ***1378 - Wolbach Library***.”  

If you are wondering how to get from the human-readable keys to machine-readable URIs, this happens after using JSON-LD’s transformation algorithms. For more information on the algorithms and what they do, check out the [JSON-LD primer](https://gitlab.com/metasat/metasat-toolkit/-/blob/master/docs/json-ld_primer.md) in the "Docs" folder of the repository.

> written by Allie Williams
