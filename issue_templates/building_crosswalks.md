# Building MetaSat crosswalks

## Crosswalk

[required]

Provide an introduction to the database, vocabulary, or dictionary that can be crosswalked to. Include a link, if possible.

## Relevance

[required]

Why is this source an important one for MetaSat to crosswalk to? How does the souce impact the satellite or astrophysics community? How widely used is it? Is it proprietary or open?

## Terms to crosswalk

[required]

Include which MetaSat terms can be crosswalked, and what terms they match with on the proposed vocabulary. Include links, if relevant. This can be done using a table:

| MetaSat | Crosswalk |
| - | - |
|MetaSat term | Crosswalked term |

If the crosswalk would be very extensive, include at least 5 example terms.
