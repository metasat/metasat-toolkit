# Adding MetaSat concepts

Please make sure to [search the existing vocabulary](https://schema.space/metasat/?view=search) before proposing a new concept. The concept you are looking for may have a different name than what you are expecting, and our search function also includes synonyms.  

If you would like to propose a change to an existing concept, please use the [Reviewing Concepts](https://gitlab.com/metasat/metasat-toolkit/-/blob/master/issue_templates/reviewing_concepts.md) template.

## Proposed new concept

[required]
    
Name the proposed new concept and give a brief description or definition. Include any synonyms or acronyms if they exist.

## Explanation

[required]

Provide more detailed introduction to the concept and why it is relevant to the MetaSat vocabulary. Does this term exist in another vocabulary, database, or dictionary? If so, please provide a link, if possible.

## Example

[optional, but encouraged]

Include one or more examples of the concept. For instance, examples for the concept "satellite" could include "ISS" and "Sputnik 1." Include units, if relevant.

## Concept segment

[optional, but encouraged]

Include which concept segments this concept could be grouped under. Current segments include:
* Ground segment
* Launch segment
* Space segment
* User segment  

Include a brief description of why the concept belongs in the chosen segment(s).

## Concept families

[optional, but encouraged]

Include which concept families this concept could be grouped under. Current families include:
* Attitude control
* Communications
* Computer hardware
* Electrical
* Instrumentation
* Mission
* Observation
* Orbital mechanics
* Person
* Product
* Propulsion
* Service
* Software
* Solid mechanics
* Structure
* Thermal control 

Include a brief description of why the concept belongs in the chosen family/families.
