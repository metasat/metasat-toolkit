# Issue Templates

This folder contains markdown templates for 3 different types of MetaSat issues. We highly encourage you to use these templates when submitting issues! The best way to use them is to download the raw [Markdown](https://docs.gitlab.com/ee/user/markdown.html) files and edit them directly in the GitLab "New Issue" page.  

- [adding_concepts.md](https://gitlab.com/metasat/metasat-toolkit/-/blob/master/issue_templates/adding_concepts.md) is a template for adding brand new concepts to MetaSat.

- [building_crosswalks.md](https://gitlab.com/metasat/metasat-toolkit/-/blob/master/issue_templates/building_crosswalks.md) is a template for proposing crosswalks to new vocabularies, databases, or dictionaries not already included in MetaSat's crosswalks.

- [reviewing_concepts.md](https://gitlab.com/metasat/metasat-toolkit/-/blob/master/issue_templates/reviewing_concepts.md) is a template for proposing changes to already existing MetaSat concepts.

For more information please visit our Contributor Guidelines page [here](https://gitlab.com/metasat/metasat-toolkit/-/blob/master/CONTRIBUTING.md).
