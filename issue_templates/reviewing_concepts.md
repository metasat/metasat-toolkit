# Reviewing MetaSat concepts

If you are proposing a new element that does not yet exist in MetaSat, please use the [Adding Concepts](https://gitlab.com/metasat/metasat-toolkit/-/blob/master/issue_templates/adding_concepts.md) template.

## Term

[required]

Name and provide a link to the concept you are reviewing. For example, [satellite](https://schema.space/metasat/satellite).

## Problem

[required]

What about this concept needs to be changed? This could include the:
* Name
* Description
* Source for the description
* Example given
* Synonym(s), including missing synonyms
* Concept segment(s)
* Concept family/families

**Note**: Concepts are only removed/deprecated very rarely. Be prepared to have a very thorough explanation if you are proposing the removal of a concept.

### Justification

[required]

Why does the concept need to be changed? What about the concept is unclear, inaccurate, or unaligned with how the satellite community understands it?

## Proposed change

[required]

What change would you make to improve the concept? How will this change make the concept clearer or more accurate?
